#!/bin/bash
#-----------------------------------------------------------------------------#
# This is a primitive "frontend" for the default iputils ping utility         |
# using ASCII graphics.                                                       |
# It converts ping values into horizontal lines of given length and thus      |
# provides a quick visual overview of current ping.                           |
#                                                                             |
# Author: gitlab.com/fejak                                                    |
#-----------------------------------------------------------------------------#
readonly ARGCNT="$#"    # number of command line arguments
readonly ARG1="$1"      # value of the first argument
#------------------------------------------------------------------------------
readonly OKARGUMENT=0
readonly BADARGUMENT=1
readonly NOARGUMENT=2
#------------------------------------------------------------------------------
readonly DEFAULT="8.8.8.8"
#------------------------------------------------------------------------------
drawLine ()
{
  local lineLen="$1"
  local drawChar="$2"

  # calling echo in every iteration may seem expensive, but:
  # 1) echo is usually a shell builtin, so there is almost no overhead
  # 2) concatenating strings in bash is extremely slow
  for i in $(seq 1 "$lineLen"); do
    echo -n "$drawChar"
  done
}
#------------------------------------------------------------------------------
drawPing ()
{
  local currPing="$1"
  local cols="$2"
  local lineLen=""

  if [ -z "$currPing" ]; then
    echo "       ERROR"
    return 1
  fi

  printf "[%4d] " "$currPing"

  lineLen=$((currPing/5)) # one dash = 5 ms of ping

  if [ "$lineLen" -gt $((cols-13)) ]; then
    drawLine $((cols-12)) ">"
  else
    drawLine "$lineLen" "-"
    echo -n "|"
  fi

  echo " $currPing"
}
#------------------------------------------------------------------------------
# get the current ping value using the default ping command
getCurrPing ()
{
  local address="$1"
  local currPing=$(ping -c1 "$address") # could check for return value here ???

  echo "$currPing" \
  | grep -o 'time=.*ms' \
  | cut -d'=' -f2 \
  | cut -d' ' -f1 \
  | cut -d'.' -f1
}
#------------------------------------------------------------------------------
runPing ()
{
  local address="$1"    # the address to ping
  local currPing=""     # the current ping value
  local cols=""         # current width of the terminal window

  while true; do
    cols=$(tput cols)   # get the current width of the terminal window
    currPing=$(getCurrPing "$address")
    drawPing "$currPing" "$cols"
    sleep 1
  done
}
#------------------------------------------------------------------------------
checkArgument ()
{
  local argCnt="$1"
  local arg1="$2"
  local retval=""

  if [ "$argCnt" -lt 1 ]; then
    return "$NOARGUMENT"
  fi

  ping -c1 -W2 "$arg1" 1>/dev/null 2>/dev/null
  retval="$?"

  if [ "$retval" -lt 2 ]; then
    return "$OKARGUMENT"
  else
    return "$BADARGUMENT"
  fi
}
#------------------------------------------------------------------------------
getIPAddress ()
{
  local address="$1"

  ping -c1 -W2 "$address" \
  | grep '^PING' \
  | awk '{print $3}' \
  | cut -d'(' -f2 \
  | cut -d')' -f1
}
#------------------------------------------------------------------------------
printMessage ()
{
  local address="$1"
  local ipAddress=$(getIPAddress "$address")

  echo -n "Pinging to $address"

  if [ "$address" != "$ipAddress" ]; then
    echo -n " ($ipAddress)"
  fi

  echo "..."
}
#------------------------------------------------------------------------------
main ()
{
  local argCnt="$1"
  local arg1="$2"
  local retval=""

  checkArgument "$argCnt" "$arg1"
  retval="$?"

  if [ "$retval" -eq "$OKARGUMENT" ]; then
    printMessage "$arg1"
    runPing "$arg1"
  elif [ "$retval" -eq "$BADARGUMENT" ]; then
    echo "Name or service not known."
    exit 1
  else #NOARGUMENT
    echo "No name or address entered. Defaulting to $DEFAULT..."
    runPing "$DEFAULT"
  fi
}
#------------------------------------------------------------------------------
main "$ARGCNT" "$ARG1"
