/*----------------------------------------------------------------------------*/
/*
 * This is an attempt to create a C version of the "Visual ping" program.
 *
 * This program provides basically the same functionality as the one written
 * in Bash. The code is quite messy and I'm planning to clean it up sometimes
 * in the future, but it should work.
 *
 * This program uses the popen function in order to access some of the programs
 * from coreutils / iputils. It's a GCC GNU extension, so the program must be
 * compiled with the -std=gnu89 flag.
 *
 * This program uses some of the functions declared in the str.h file which is
 * basically a primitive library for a little bit more convenient string
 * handling (IMO).
 *
 * Author: gitlab.com/fejak
 *
 * Compilable with:
 *   gcc -Wall -pedantic -Wextra -std=gnu89 -O0 visual-ping.c
 *
 * TODO: code cleanup, add statistics, possibly add some ncurses interface
 */
/*----------------------------------------------------------------------------*/
#include "str.h"      /*  str, strinit, strappend, strreset, strfree,
                          strremovelast */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>   /*  strerror, strcmp */
#include <errno.h>    /*  errno */
#include <unistd.h>   /*  sleep */
/*----------------------------------------------------------------------------*/
#define BOOL        int
#define TRUE        1
#define FALSE       0
/*----------------------------------------------------------------------------*/
#define DASH_SIZE   5     /*  how many ms of ping one ASCII dash represents */
#define DELAY       1     /*  delay between pings in seconds */
/*----------------------------------------------------------------------------*/
#define DEFAULT     "8.8.8.8"
/*----------------------------------------------------------------------------*/
/**
 * Read the cmdFile stream until EOF and store its content into cmdStr.
 */
void storeOutput ( FILE * cmdFile, str * cmdStr )
{
  while ( TRUE )
  {
    int c = fgetc ( cmdFile );
    if ( c == EOF )
      break;
    stradd ( cmdStr, c );
  }
}
/*----------------------------------------------------------------------------*/
void createTputColsCmd ( str * cmdStr )
{
  strappend ( cmdStr, "tput cols" );
}
/*----------------------------------------------------------------------------*/
/**
 * Draw line of given length + numeric ping values.
 */
void drawLine ( int currPing, int termWidth )
{
  int length = currPing / DASH_SIZE,
      i;
  char c = '-';
  str s;
  strinit ( &s );

  if ( length > termWidth - 13 )
  {
    length = termWidth - 12;
    c = '>';
  }

  for ( i = 0; i < length; ++i )
  {
    stradd ( &s, c );
  }

  printf ( "[%4d] %s%s %d\n", currPing, s . str_, c == '-' ? "|" : "", currPing );

  fflush ( stdout ); /* in order for tee to work */

  strfree ( &s );
}
/*----------------------------------------------------------------------------*/
/**
 * Create a ping command to a given address.
 */
void createPingCmd ( str * cmdStr )
{
  str address;
  strinit ( & address );
  strappend ( & address, cmdStr -> str_ );

  strreset ( cmdStr );

  strappend ( cmdStr, "ping -c1 " );
  strappend ( cmdStr, address . str_ );
  strappend ( cmdStr, " 2>/dev/null" );

  strfree ( & address );
}
/*----------------------------------------------------------------------------*/
/**
 * Create an echo command of given string in order to process further in a pipe.
 * TOFIX: Error when given cmdStr contains a single quote character.
 */
void createEchoCmd ( str * cmdStr )
{
  str cmdStrBckp;
  strinit   ( & cmdStrBckp );
  strappend ( & cmdStrBckp, cmdStr -> str_ );
  strreset  ( cmdStr );

  strappend ( cmdStr, "echo '" );
  strappend ( cmdStr, cmdStrBckp . str_ );
  strappend ( cmdStr, "'" );
  strappend ( cmdStr, " | grep -o 'time=.*ms'" );
  strappend ( cmdStr, " | cut -d'=' -f2" );
  strappend ( cmdStr, " | cut -d' ' -f1" );
  strappend ( cmdStr, " | cut -d'.' -f1" );

  strfree ( & cmdStrBckp );
}
/*----------------------------------------------------------------------------*/
/**
 * Handle (error) return values of ping >0.
 */
void handleErrorMsg ( int retval )
{
  printf ( "       " );

  switch ( retval )
  {
    case 256: printf ( "TIMEOUT" ); break;
    case 512: printf ( "NETWORK UNREACHABLE" ); break;
    default:  printf ( "ERROR" ); break;
  }

  printf ( "\n" );
  fflush ( stdout ); /* in order for tee to work */
}
/*----------------------------------------------------------------------------*/
/**
 * Basically the core function of the program.
 * It executes a given shell command and stores the output into the cmdStr var.
 * Input: 1) Pointer to a function which creates a shell command
 *        2) Pointer to a str variable
 * The function creates a string of a given shell command and stores it into
 * the cmdStr str variable. Then executes the command using the popen function.
 * Output of the popen function is stored in a stream pointed to by the FILE
 * variable. We store the content of the stream into the cmdStr variable.
 * In the end we close the stream.
 */
int runCommand ( void ( * createCmdFunc ) ( str * ), str * cmdStr )
{
  FILE * cmdFile = NULL;

  createCmdFunc ( cmdStr ); /* passed in as function parameter */

  if ( ( cmdFile = popen ( cmdStr -> str_, "r" ) ) == NULL )
  {
    return -1;
  }

  strreset ( cmdStr );

  /* store output of the command from the file to the string */
  storeOutput ( cmdFile, cmdStr );

  return pclose ( cmdFile ); /* -1 => fail */
}
/*----------------------------------------------------------------------------*/
/**
 * Main program loop.
 * 1) Create a ping command to a given address.
 * 2) Execute the command.
 * 3) Check the return value.
 * 4) Filter the raw ping value from the output of the ping command.
 * 5) Read the current width of the terminal window.
 * 6) Draw the output line of a given length.
 */
BOOL pingLoop ( char const * address )
{
  int retval,
      currPing,
      termWidth;

  str cmdStr;
  strinit ( & cmdStr );

  while ( 1 )
  {
    strreset ( & cmdStr );

    strappend ( & cmdStr, address );

    retval = runCommand ( createPingCmd, & cmdStr );

    if ( retval == -1 )
    {
      strfree  ( & cmdStr );
      return FALSE;
    }
    else
    if ( retval != 0 )
    {
      handleErrorMsg ( retval );
      sleep ( 1 );
      continue;
    }

    strremovelast ( & cmdStr );

    if ( runCommand ( createEchoCmd, & cmdStr ) == -1 )
    {
      strfree ( & cmdStr );
      return FALSE;
    }

    currPing = atoi ( cmdStr . str_ );

    strreset ( & cmdStr );

    if ( runCommand ( createTputColsCmd, & cmdStr ) == -1 )
    {
      strfree ( & cmdStr );
      return FALSE;
    }

    termWidth = atoi ( cmdStr . str_ );

    drawLine ( currPing, termWidth );

    sleep ( DELAY );
  }

  strfree ( & cmdStr );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/**
 * Tests ping to a given address and returns TRUE when everything is OK.
 */
BOOL testPing ( str * address )
{
  return runCommand ( createPingCmd, address ) == 0;
}
/*----------------------------------------------------------------------------*/
/**
 * Create a command to filter the IP address from the ping command output.
 */
void createEchoCmdFilterAddress ( str * pingOutput )
{
  str tmpStr;
  strinit ( & tmpStr );

  strappend ( & tmpStr, pingOutput -> str_ );
  strreset ( pingOutput );

  strappend ( pingOutput, "echo '" );
  strappend ( pingOutput, tmpStr . str_ );
  strappend ( pingOutput, "'" );
  strappend ( pingOutput, "| grep '^PING'" );
  strappend ( pingOutput, "| awk '{print $3}'" );
  strappend ( pingOutput, "| cut -d'(' -f2" );
  strappend ( pingOutput, "| cut -d')' -f1" );

  strfree ( & tmpStr );
}
/*----------------------------------------------------------------------------*/
/*/
 * Filter the IP address from the ping command output.
 */
BOOL getIpAddress ( str * pingOutput )
{
  return runCommand ( createEchoCmdFilterAddress, pingOutput ) != -1;
}
/*----------------------------------------------------------------------------*/
int main ( int argc, char * argv [] )
{
  str address,    /* the address to pass to the ping command */
      ipAddress;  /* potential derived IP address */

  strinit ( & address );
  strinit ( & ipAddress );

  if ( argc < 2 )
  {
    strappend ( & address, DEFAULT );
    printf ( "No name or address entered. Defaulting to %s...\n", DEFAULT );
    strfree ( & ipAddress );  /* ipAddress not used */
  }
  else
  {
    strappend ( & address, argv [1] );
    strappend ( & ipAddress, address . str_ );
    /*  input   - address,
        output  - output of the ping command */
    if ( ! testPing ( & ipAddress ) )
    {
      printf ( "Name or service not known.\n" );
      strfree ( & address );
      strfree ( & ipAddress );
      return EXIT_FAILURE;
    }
    else
    {
      if ( ! getIpAddress ( & ipAddress ) )
      {
        strfree ( & address );
        strfree ( & ipAddress );
        return FALSE;
      }
      strremovelast ( & ipAddress );
      printf ( "Pinging to %s", address . str_ );
      if ( strcmp ( address . str_, ipAddress . str_ ) != 0 )
      {
        printf ( " (%s)", ipAddress . str_ );
      }
      printf ( "...\n" );
      strfree ( & ipAddress );
    }
  }

  if ( ! pingLoop ( address . str_ ) )
  {
    strerror ( errno );
    strfree ( & address );
    return EXIT_FAILURE;
  }

  strfree ( & address );
  return EXIT_SUCCESS;
}
/*----------------------------------------------------------------------------*/
