#ifndef STR_H__Gg33pRHgey23xeqohZYcr8vcI
#define STR_H__Gg33pRHgey23xeqohZYcr8vcI
/*----------------------------------------------------------------------------*/
#define INIT_SIZE         1
#define RESIZE_FACTOR     2
/*----------------------------------------------------------------------------*/
#define BOOL              int
#define TRUE              1
#define FALSE             0
/*----------------------------------------------------------------------------*/
typedef struct str
{
  char  * str_;
  int   all_;
  int   len_; /* length without the terminating '\0' */
} str;
/*============================================================================*/
/* START OF HELPER FUNCTIONS - not to be used by the user directly */
void stralloc ( str * s );
/* END OF HELPER FUNCTIONS */
/*============================================================================*/
/* init MUST be called as the first function after the declaration of str */
void strinit ( str * s );
/*----------------------------------------------------------------------------*/
void strfree ( str * s );
/*----------------------------------------------------------------------------*/
void strreset ( str * s );
/*----------------------------------------------------------------------------*/
int strlength ( str const * s );
/*----------------------------------------------------------------------------*/
/* add a character */
void stradd ( str * s, char c );
/*----------------------------------------------------------------------------*/
/* append a string */
void strappend ( str * s1, char const * s2 );
/*----------------------------------------------------------------------------*/
/* remove char at given index */
BOOL strremoveidx ( str * s, int idx );
/*----------------------------------------------------------------------------*/
/* remove the last char */
BOOL strremovelast ( str * s );
/*----------------------------------------------------------------------------*/
#endif /* STR_H__Gg33pRHgey23xeqohZYcr8vcI */
