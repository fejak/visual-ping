/*----------------------------------------------------------------------------*/
#include "str.h"

#include <stdio.h>
#include <stdlib.h>
/*============================================================================*/
/* START OF HELPER FUNCTIONS - not to be used by the user directly */
void stralloc ( str * s )
{
  if ( s -> all_ == 0 )
  {
    s -> all_ = INIT_SIZE;
    s -> str_ = malloc ( sizeof ( * s -> str_ ) * s -> all_ );
  }
  else
  if ( s -> all_ == s -> len_ + 1 )
  {
    s -> all_ *= RESIZE_FACTOR;
    s -> str_ = realloc ( s -> str_, sizeof ( * s -> str_ ) * s -> all_ );
  }
  else
  if ( s -> all_ > ( s -> len_ + 1 ) * RESIZE_FACTOR )
  {
    s -> all_ /= RESIZE_FACTOR;
    s -> str_ = realloc ( s -> str_, sizeof ( * s -> str_ ) * s -> all_ );
  }
}
/* END OF HELPER FUNCTIONS */
/*============================================================================*/
/* init MUST be called as the first function after the declaration of str */
void strinit ( str * s )
{
  s -> str_ = NULL;
  s -> all_ = 0;
  s -> len_ = 0;

  /* add terminating '\0' */
  stralloc ( s );
  s -> str_ [s -> len_] = '\0';
}
/*----------------------------------------------------------------------------*/
void strfree ( str * s )
{
  free ( s -> str_ );
  s -> str_ = NULL;
}
/*----------------------------------------------------------------------------*/
void strreset ( str * s )
{
  strfree ( s );
  strinit ( s );
}
/*----------------------------------------------------------------------------*/
int strlength ( str const * s )
{
  return s -> len_;
}
/*----------------------------------------------------------------------------*/
/* add a character */
void stradd ( str * s, char c )
{
  if ( c == '\0' )
    return;

  if ( s -> len_ == 0 )
    stralloc ( s );

  s -> str_ [s -> len_] = c;
  ++ s -> len_;

  /* add terminating '\0' */
  stralloc ( s );
  s -> str_ [s -> len_] = '\0';
}
/*----------------------------------------------------------------------------*/
/* append a string */
void strappend ( str * s1, char const * s2 )
{
  int i = 0;
  while ( s2 [i] != '\0' )
  {
    stradd ( s1, s2 [i] );
    ++i;
  }
}
/*----------------------------------------------------------------------------*/
BOOL strremoveidx ( str * s, int idx )
{
  int i;

  if ( idx < 0 || idx >= s -> len_ )
    return FALSE;

  for ( i = idx; i < s -> len_ + 1; ++i )
  {
    s -> str_ [i] = s -> str_ [i + 1];
  }
  -- s -> len_;
  stralloc ( s );

  return TRUE;
}
/*----------------------------------------------------------------------------*/
BOOL strremovelast ( str * s )
{
  return strremoveidx ( s, s -> len_ - 1 );
}
/*----------------------------------------------------------------------------*/
