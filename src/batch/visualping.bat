@echo off
rem ----------------------------------------------------------------------------
rem This is a DOS/batch version of the "visual ping" program.
rem I had no previous experience in DOS batch scripting.
rem Honestly, I did not enjoy it much.
rem But the program somehow works, although it's very slow.
rem Version 0.1

rem TODO: add IP address parameter, improve performance, refactor, add comments

rem Author: gitlab.com/fejak

rem The following StackOverflow snippets were used in this program:
rem superuser.com/questions/1504376
rem stackoverflow.com/questions/2768608
rem stackoverflow.com/questions/11419046
rem stackoverflow.com/questions/14978548
rem stackoverflow.com/questions/19006067
rem stackoverflow.com/questions/25066360
rem ----------------------------------------------------------------------------
set DASH_SIZE=5
rem the ip address is hardwired here
set IP_ADDRESS=8.8.8.8
set TIMEOUT=2
set TIMEOUT_ERROR=5
set "CHAR_OK=-"
set "CHAR_ERROR=X"

goto SUB_main
rem ----------------------------------------------------------------------------
rem this subroutine provides basic 'grep' functionality
rem arg[in]  ... command
rem arg[in]  ... keyword
rem arg[out] ... filtered line
:SUB_filterLine
    set command=%~1
    set keyword=%~2

    set %3=FAIL

    rem caret ^ ... escape the next character
    for /f "delims=" %%i in ('%command% ^| find "%keyword%"') do set %3=%%i

    exit /b
rem ----------------------------------------------------------------------------
rem run the ping command + filter the line containing "TTL"
rem arg[in]  ... ip address
rem arg[out] ... ping command output; line containing the "TTL" keyword
:SUB_getPingLine
    rem source: SO 2768608
    set ipAddress=%~1

    call :SUB_filterLine "ping -n 1 %IP_ADDRESS%" "TTL" output

    set %2=%output%
    exit /b
rem ----------------------------------------------------------------------------
rem this subroutine provides basic 'cut' functionality
rem cut the 7th column using the space delimiter
:SUB_getPingMs
    rem source: SO 25066360
    set pingLine=%~1
    for /f "tokens=7,* delims= " %%i in ('echo %pingLine%') do set %2=%%i
    exit /b
rem ----------------------------------------------------------------------------
:SUB_getPingRaw
    rem filter the current ping
    set pingMs=%~1
    for /f "tokens=1,* delims=ms" %%i in ('echo %pingMs%') do set %2=%%i
    exit /b
rem ----------------------------------------------------------------------------
rem get the current terminal window width
rem source: SO 14978548
rem source: SO 11419046
:SUB_getTermWidth
    for /f "usebackq tokens=2* delims=: " %%w in (`mode con ^| findstr Columns`) do set %1=%%w
    exit /b
rem ----------------------------------------------------------------------------
rem get length of the ping line
:SUB_getLength
    set pingRaw=%~1

    rem "set arithmetics"
    set /a length=(pingRaw+DASH_SIZE-1)/DASH_SIZE

    set %2=%length%
    exit /b
rem ----------------------------------------------------------------------------
rem count the digits of the ping number value in order to calculate the offset
:SUB_countDigits
    setlocal enabledelayedexpansion
    set pingRaw=%~1

    :countDigits
        if %pingRaw% gtr 0 (
            set /a pingRaw=pingRaw/10
            set /a digitCnt=digitCnt+1
            goto countDigits
        )

    rem "tunneling"
    rem source: SO 1504376
    endlocal & set %2=%digitCnt%
    exit /b
rem ----------------------------------------------------------------------------
rem calculate the number of spaces preceding the ping number value
:SUB_calculatePadding
    setlocal enabledelayedexpansion
    set pingRaw=%~1

    call :SUB_countDigits "%pingRaw%" digitCnt

    set /a repeatCnt=4-digitCnt

    for /l %%x in (1, 1, %repeatCnt%) do (
        set "spaces=!spaces! "
    )

    endlocal & set %2=%spaces%
    exit /b
rem ----------------------------------------------------------------------------
:SUB_drawLine
    setlocal enabledelayedexpansion
    set pingRaw=%~1
    set drawChar=%CHAR_OK%
    set "str="

    call :SUB_calculatePadding "%pingRaw%" spaces
    call :SUB_getTermWidth termWidth
    call :SUB_getLength "%pingRaw%" length

    set /a limitSize=termWidth-13

    rem echo without the trailing newline
    echo|set /p="[%spaces%%pingRaw%] "

    rem ------------------------------------------------------------------------
    if %length% gtr %limitSize% goto pingOverflow

        for /l %%x in (1, 1, %length%) do (
            set "str=!str!%drawChar%"
        )

        echo|set /p="%str%|"

        goto drawLineEnd
    rem ------------------------------------------------------------------------
    :pingOverflow

        set drawChar=%CHAR_ERROR%
        set /a length=%limitSize%+1

        for /l %%x in (1, 1, %length%) do (
            set "str=!str!%drawChar%"
        )

        echo|set /p="%str%"

        goto drawLineEnd
    rem ------------------------------------------------------------------------
    :drawLineEnd

        echo. %pingRaw%
        endlocal
        exit /b
rem ----------------------------------------------------------------------------
:SUB_main
    setlocal enabledelayedexpansion
    echo Pinging %IP_ADDRESS%...
    :loop
        call :SUB_getPingLine "%IP_ADDRESS%" pingLine
        if "%pingLine%"=="FAIL" goto error
            call :SUB_getPingMs "%pingLine%" pingMs
            call :SUB_getPingRaw "%pingMs%" pingRaw
            call :SUB_drawLine "%pingRaw%"
            timeout %TIMEOUT% > nul
            goto loop
    :error
        echo.       ERROR
        timeout %TIMEOUT_ERROR% > nul
        goto loop
        rem source: SO 19006067
    endlocal
exit /b
rem ----------------------------------------------------------------------------
