# Visual ping

A primitive ASCII-based "frontend" for the default <a href="https://github.com/iputils/iputils" target="_blank" rel="noopener noreferrer nofollow">iputils</a> ping utility written in GNU Bash + GNU utilities.
Raw numeric values are converted to horizontal lines of given length and thus draw a graph which gives a quick visual overview of current ping.

## Usage

```
./visual-ping.sh [url_or_ip_address]
```
* The argument is optional; in case of missing argument the default address will be pinged (8.8.8.8 set by default in the script).

## Example of output

```
user@host:~$ ./visual-ping.sh
No name or address entered. Defaulting to 8.8.8.8...
[  11] --| 11
[   9] -| 9
[  19] ---| 19
[  12] --| 12
[  16] ---| 16
[  11] --| 11
[  11] --| 11
[2789] >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 2789
[  11] --| 11
[  10] --| 10
[  11] --| 11
[  12] --| 12
[  93] ------------------| 93
[  16] ---| 16
       ERROR
[   9] -| 9
[  11] --| 11
```

## Note

* This is a simple toy "project" which serves to me as a way to learn various things about git / gitlab.
* A C version was added too.
* A DOS/batch version was added too.

## TODO

* Rewrite in pure sh???
